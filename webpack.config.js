const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports ={
    entry:['babel-polyfill','./src/js/index.js'],
    output: {
        filename: "app.bundle.js",
        path: path.resolve(__dirname,'dist/js'),
    },
    module: {
        rules: [
            {
                test:/\.vue$/,
                use: "vue-loader"
            },
            {
                test: /\.s[a|c]ss$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test:/\.js$/,
                use:'babel-loader'
            }
        ],

    },

    mode: "development",


    plugins: [
        new VueLoaderPlugin(),
    ]
};