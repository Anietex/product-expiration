<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <title>Dashboard</title>
</head>
<body>
<div id="app">
    <header>
        <nav class=" light-blue accent-4">
            <div class="nav-wrapper container ">
                <a href="#" class="brand-logo">PEMS</a>
            </div>
        </nav>
    </header>
    <main class="">
<!--        <div class="header-text white z-depth-1">-->
<!--           <div class="container">-->
<!--               <h2><span class="fa fa-cog"></span> Dashboard <small>Manage your products</small></h2>-->
<!--           </div>-->
<!--        </div>-->
        <div class="buttons ">
            <div class="container">
                <div class="row">
                    <div class="col s4 m3 lg3">
                        <button class="waves-effect waves-light btn red lighten-2 modal-trigger" data-target="batch-modal">New Batch</button>
                    </div>
                    <div class="col m3 s6 lg3">
                        <button class="waves-effect waves-light btn modal-trigger" data-target="product-modal">New Product</button>
                    </div>
                    <div class="col s6 m3 lg3">
                        <button class="waves-effect waves-light btn red lighten-2" onclick="sendEvent()">Product Batches</button>
                    </div>
                    <div class="col s6  m3 lg3">
                        <button class="waves-effect waves-light btn">Product List</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="summaries">
            <div class="container">
                <div class="row">
                    <div class="col m4 s1 l4">
                        <div class="summary red darken-3 z-depth-4">
                            <div class="header">
                                <h1><span class="fa fa-balance-scale"></span> 50</h1>
                            </div>
                            <hr>
                            <div class="footer right-align">
                                <p>Total Products</p>
                            </div>
                        </div>
                    </div>

                    <div class="col m4 s1 l4">
                        <div class="summary blue lighten-2 z-depth-4">
                            <div class="header">
                                <h1><span class="fa fa-balance-scale"></span> 50</h1>
                            </div>
                            <hr>
                            <div class="footer right-align">
                                <p>Total Batches</p>
                            </div>
                        </div>
                    </div>

                    <div class="col m4 s1 l4">
                        <div class="summary green accent-2 z-depth-4">
                            <div class="header">
                                <h1><span class="fa fa-balance-scale"></span> 50</h1>
                            </div>
                            <hr>
                            <div class="footer">
                                <p class="left">Value: $50000</p>
                                <p class="right">Products Expiring Soon</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table">
            <div class="container">
                <div class="z-depth-1 white t-wrapper">
                    <div class="title">
                        <h5>Products Expiring Soon</h5>
                    </div>
                    <div class="table-header">

                    </div>
                    <table>
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Product Name</th>
                            <th>Bar Code</th>
                            <th>Production Date</th>
                            <th>Expiry Date</th>
                            <th>Qty</th>
                            <th>Price</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td>2</td>
                            <td>Ovaltine 500mg</td>
                            <td>19309404904994</td>
                            <td>12/06/2015</td>
                            <td>12/06/2015</td>
                            <td>100</td>
                            <td>N200</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Ovaltine 500mg</td>
                            <td>19309404904994</td>
                            <td>12/06/2015</td>
                            <td>12/06/2015</td>
                            <td>100</td>
                            <td>N200</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
</div>


<!--Batch Modal-->

<div id="batch-modal" class="modal">
    <form action="">
        <div class="modal-content">
            <h4>Add New Product Batch</h4>
            <div class="row">
                <div class="input-field col s12">
                    <input id="batch_id" type="number" class="validate" required>
                    <label for="batch_id">Batch ID</label>
                </div>
                <div class="input-field col s12">
                    <input id="batch_name" type="text" class="validate" required>
                    <label for="batch_name">Batch Name</label>
                </div>
                <div class="input-field col s12">
                    <input id="created_by" type="text" class="validate" required>
                    <label for="created_by">Created By</label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="modal-close waves-effect waves-green btn-flat">Cancel</button>
            <button type="submit" class=" waves-effect waves-green btn-flat">Add</button>
        </div>
    </form>
</div>

<!--Product modal-->
<div id="product-modal" class="modal">
    <form>
        <div class="modal-content">
            <h4>Add New Product</h4>
            <div class="row">
                <div class="input-field col s12 m6">
                    <select id="batch">
                        <option value="" disabled selected>Select Batch</option>
                    </select>
                    <label for="batch">Product Batch</label>
                </div>

                <div class="input-field col s12 m6">
                    <input id="product_name" type="text" class="validate" required>
                    <label for="product_name">Product Name</label>
                </div>




                <div class="input-field col s12 m6">
                    <input id="manufacturer" type="text" class="validate" required>
                    <label for="manufacturer">Manufacturer Name</label>
                </div>

                <div class="input-field col s12 m6">
                    <input id="bar_code" type="text" class="validate" required>
                    <label for="bar_code">Bar Code</label>
                </div>
                <div class="input-field col s12 m6">
                    <input id="quantity" type="number" class="validate" required>
                    <label for="quantity">Quantity</label>
                </div>
                <div class="input-field col s12 m6">
                    <input id="price" type="number" class="validate" required>
                    <label for="price">Price</label>
                </div>
                <div class="input-field col s12 m6">
                    <input id="man_date" type="date" class="validate" required>
                    <label for="man_date">Manufacturing Date</label>
                </div>
                <div class="input-field col s12 m6">
                    <input id="expiry_date" type="date" class="validate" required>
                    <label for="expiry_date">Expiry Date</label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="modal-close waves-effect waves-green btn-flat">Add</button>
        </div>
    </form>
</div>







<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="js/vue.js"></script>
<script type="text/javascript" src="js/vue-router.js"></script>
<script src="js/axios.min.js" type="text/javascript"></script>
<script src="js/app.js" type="text/javascript"></script>
</body>
</html>